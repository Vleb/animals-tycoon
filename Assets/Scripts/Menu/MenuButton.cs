﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public abstract class MenuButton : MonoBehaviour
{
    public AudioSource click0;
    public AudioSource click1;
    public AudioSource click2;

    public bool exitScene = true;

    public AudioSource music;
    public Image fading;
    public bool eject = true;

    private float ejectionSpeed = 1f;
    private float fadeSpeed = .05f;

    protected abstract void ButtonAction();

    public IEnumerator EjectButton()
    {
        // Button animation
        if (eject)
        {
            RectTransform bt = GetComponent<RectTransform>();
            for (float bx = bt.anchoredPosition.x, multiplier = 1f; bx + bt.rect.width > -Screen.width / 2f; bx -= ejectionSpeed * multiplier, multiplier *= 1.05f)
            {
                bt.anchoredPosition = new Vector2(bx, bt.anchoredPosition.y);
                yield return null;
            }
        }

        // Fadeout
        Image fi = fading.GetComponent<Image>();
        float mv = music.volume;
        for (float fa = fi.color.a; fa < 1f; fa += fadeSpeed)
        {
            fi.color = new Color(fi.color.r, fi.color.g, fi.color.b, fa);
            music.volume = (1f - fa) * mv;
            yield return new WaitForSeconds(.01f);
        }
        fi.color = new Color(fi.color.r, fi.color.g, fi.color.b, 1f);
        music.volume = 0f;

        // Execute button action at the end of the animation
        ButtonAction();
    }

    public void ButtonClick()
    {
        // Play random click sound
        int randomSound = Random.Range(0, 3);
        if (randomSound == 0)
        {
            click0.Play(0);
        }
        else if (randomSound == 1)
        {
            click1.Play(0);
        }
        else
        {
            click2.Play(0);
        }

        if (exitScene)
        {
            // Disable all the buttons
            var blist = FindObjectsOfType<Button>();
            for (int i = 0; i < blist.Length; i++)
            {
                blist[i].interactable = false;
            }

            // Play the button animation and action
            StartCoroutine("EjectButton");
        } 
        else
        {
            ButtonAction();
        }
    }
}
