﻿using UnityEngine;

public class MenuButtonQuit : MenuButton
{
    protected override void ButtonAction()
    {
        Application.Quit();
    }
}
