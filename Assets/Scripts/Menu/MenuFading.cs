﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MenuFading : MonoBehaviour
{
    private float fadeSpeed = 0.05f;

    IEnumerator FadeIn()
    {
        Image fi = GetComponent<Image>();
        for (float fa = 1.0f; fa > 0.0f; fa -= fadeSpeed)
        {
            fi.color = new Color(fi.color.r, fi.color.g, fi.color.b, fa);
            yield return new WaitForSeconds(.01f);
        }
        fi.color = new Color(fi.color.r, fi.color.g, fi.color.b, .0f);
    }

    void Start()
    {
        StartCoroutine("FadeIn");
    }
}
