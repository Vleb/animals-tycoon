﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EntryFading : MonoBehaviour
{
    private float entryDuration = 3.0f;
    private float fadeSpeed = 0.01f;

    IEnumerator EntryFade()
    {
        yield return new WaitForSeconds(entryDuration);
        Image fi = GetComponent<Image>();
        for (float fa = 0.0f; fa < 1.0f; fa += fadeSpeed)
        {
            fi.color = new Color(fi.color.r, fi.color.g, fi.color.b, fa);
            yield return new WaitForSeconds(.01f);
        }
        fi.color = new Color(fi.color.r, fi.color.g, fi.color.b, 1.0f);
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    void Start()
    {
        StartCoroutine("EntryFade");
    }
}
