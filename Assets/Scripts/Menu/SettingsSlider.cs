﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsSlider : MonoBehaviour
{
    public bool isResolution = false;
    public bool isQuality = false;
    public bool isSound = false;
    public bool isSensitivity = false;
    public Text feedback;

    private Slider slider;
    private string[] qualityTags = new string[] { "Low", "Medium", "High" };

    void Start()
    {
        slider = GetComponent<Slider>();
        if (isResolution)
        {
            Resolution[] resolutions = Screen.resolutions;
            int resolutionsCount = resolutions.Length - 1;
            int currentResolutionIdx = PlayerPrefs.HasKey("settingsResolution") ? PlayerPrefs.GetInt("settingsResolution") : resolutionsCount;
            slider.maxValue = resolutionsCount;
            slider.value = currentResolutionIdx;
            Resolution currentResolution = resolutions[currentResolutionIdx];
            feedback.text = currentResolution.ToString();
        }
        else if (isQuality)
        {
            int currentQuality = PlayerPrefs.HasKey("settingsQuality") ? PlayerPrefs.GetInt("settingsQuality") : 1;
            slider.value = currentQuality;
            feedback.text = qualityTags[currentQuality];
        }
        else if (isSound)
        {
            int currentVolume = PlayerPrefs.HasKey("settingsSound") ? PlayerPrefs.GetInt("settingsSound") : 50;
            slider.value = currentVolume;
            feedback.text = currentVolume.ToString() + '%';
        }
        else if (isSensitivity)
        {
            int currentSensitivity = PlayerPrefs.HasKey("settingsSensitivity") ? PlayerPrefs.GetInt("settingsSensitivity") : 3;
            slider.value = currentSensitivity;
            feedback.text = currentSensitivity.ToString();
        }
    }

    public void SliderChange()
    {
        if (isResolution)
        {
            Resolution selectedResolution = Screen.resolutions[(int)slider.value];
            feedback.text = selectedResolution.ToString();
        }
        else if (isQuality)
        {
            int selectedQuality = (int)slider.value;
            feedback.text = qualityTags[selectedQuality];
        }
        else if (isSound)
        {
            int selectedVolume = (int)slider.value;
            feedback.text = selectedVolume.ToString() + '%';
        }
        else if (isSensitivity)
        {
            int selectedSensitivity = (int)slider.value;
            feedback.text = selectedSensitivity.ToString();
        }
    }
}
