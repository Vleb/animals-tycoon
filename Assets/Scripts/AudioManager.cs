﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    void Start()
    {
        if (PlayerPrefs.HasKey("settingsSound"))
        {
            float selectedVolume = PlayerPrefs.GetInt("settingsSound") / 100.0f;
            GetComponent<AudioSource>().volume = selectedVolume;
        }
        else
        {
            GetComponent<AudioSource>().volume = 0.5f;
        }
    }
}
