using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    public GameObject openShopButton;
    public GameObject shopPanel;
    public bool open;


    // Start is called before the first frame update
    void Start()
    {
        closeShop();
    }

    void openShop()
    {
        openShopButton.SetActive(true);
        shopPanel.SetActive(false);
        open = true;
    }

    void closeShop()
    {
        openShopButton.SetActive(false);
        shopPanel.SetActive(true);
        open = false;
    }
}
