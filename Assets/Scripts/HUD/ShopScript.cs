using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScript : MonoBehaviour
{
    public int unlockPrice, roomPrice, foodPrice, medicinePrice;
    public Text unlockedRabbit, unlockedMouse;
    public Text roomCat, roomDog, roomRabbit, roomMouse;
    public Text foodCat, foodDog, foodRabbit, foodMouse;
    public Text medicine;

    public Button unlockedRabbitButton, unlockedMouseButton;
    public Button roomRabbitButton, roomMouseButton;
    public Button foodRabbitButton, foodMouseButton;


    // Update is called once per frame
    void Update()
    {
        unlockedRabbit.gameObject.SetActive(GameManager.game.data.rabbitUnlocked);
        unlockedMouse.gameObject.SetActive(GameManager.game.data.mouseUnlocked);

        unlockedRabbitButton.gameObject.SetActive(!GameManager.game.data.rabbitUnlocked);
        unlockedMouseButton.gameObject.SetActive(!GameManager.game.data.mouseUnlocked);

        roomCat.text = GameManager.game.data.catRoom.ToString();
        roomDog.text = GameManager.game.data.dogRoom.ToString();
        roomRabbit.text = GameManager.game.data.rabbitRoom.ToString();
        roomMouse.text = GameManager.game.data.mouseRoom.ToString();

        foodCat.text = GameManager.game.data.catFood.ToString();
        foodDog.text = GameManager.game.data.dogFood.ToString();
        foodRabbit.text = GameManager.game.data.rabbitFood.ToString();
        foodMouse.text = GameManager.game.data.mouseFood.ToString();

        medicine.text = GameManager.game.data.medicine.ToString();

        if (GameManager.game.data.rabbitUnlocked == true) {
            roomRabbit.gameObject.SetActive(true);
            roomRabbitButton.gameObject.SetActive(true);
            foodRabbit.gameObject.SetActive(true);
            foodRabbitButton.gameObject.SetActive(true);
        } else {
            roomRabbit.gameObject.SetActive(false);
            roomRabbitButton.gameObject.SetActive(false);
            foodRabbit.gameObject.SetActive(false);
            foodRabbitButton.gameObject.SetActive(false);
        }

        if (GameManager.game.data.mouseUnlocked == true) {
            roomMouse.gameObject.SetActive(true);
            roomMouseButton.gameObject.SetActive(true);
            foodMouse.gameObject.SetActive(true);
            foodMouseButton.gameObject.SetActive(true);
        } else {
            roomMouse.gameObject.SetActive(false);
            roomMouseButton.gameObject.SetActive(false);
            foodMouse.gameObject.SetActive(false);
            foodMouseButton.gameObject.SetActive(false);
        }
    }

    public void unlock(string type)
    {
        if (GameManager.game.data.money >= unlockPrice) {
            if (type == "rabbit") {
                GameManager.game.data.rabbitUnlocked = true;
            } else if (type == "mouse") {
                GameManager.game.data.mouseUnlocked = true;
            }
            GameManager.game.data.money -= unlockPrice;
        }
    }

    public void onBuyRoom(string type)
    {
        switch (type)
        {
            case "cat":
                buyRoom(ref GameManager.game.data.catRoom, 8);
                break;
            case "dog":
                buyRoom(ref GameManager.game.data.dogRoom, 8);
                break;
            case "rabbit":
                if (GameManager.game.data.rabbitUnlocked == true) {
                    buyRoom(ref GameManager.game.data.rabbitRoom, 4);
                } else {
                    Debug.Log("jjjjjjjj");
                }
                break;
            case "mouse":
                if (GameManager.game.data.mouseUnlocked == true) {
                    buyRoom(ref GameManager.game.data.mouseRoom, 3);
                }
                break;
            default:
                break;
        }
    }

    public void buyRoom(ref int data, int max)
    {
        if (data < max) {
            if (data < max && GameManager.game.data.money >= roomPrice) {
                GameManager.game.data.money -= roomPrice;
                data++;
            }
        }
    }

    public void onBuyFood(string type)
    {
        switch (type)
        {
            case "cat":
                buyFood(ref GameManager.game.data.catFood);
                break;
            case "dog":
                buyFood(ref GameManager.game.data.dogFood);
                break;
            case "rabbit":
                if (GameManager.game.data.rabbitUnlocked == true) {
                    buyFood(ref GameManager.game.data.rabbitFood);
                }
                break;
            case "mouse":
                if (GameManager.game.data.mouseUnlocked == true) {
                    buyFood(ref GameManager.game.data.mouseFood);
                }
                break;
            default:
                break;
        }
    }

    public void buyFood(ref int data)
    {
        if (GameManager.game.data.money >= foodPrice) {
            GameManager.game.data.money -= foodPrice;
            data++;
        }
    }

    public void buyMedicine()
    {
        if (GameManager.game.data.medicine < 1000) {
            if (GameManager.game.data.money >= medicinePrice) {
                GameManager.game.data.money -= medicinePrice;
                GameManager.game.data.medicine++;
            }
        }
    }
}
