using UnityEngine;
using UnityEngine.UI;

public class AnimalDetails : MonoBehaviour
{
    public GameObject detailPanel;

    public Text detailName, detailAge, detailBreed;
    public Image detailPicture, detailSex;
    public Sprite maleIcon, femaleIcon;
    public Slider healthBar, foodBar, hygieneBar;
    public Button healButton, feedButton, washButton, acceptButton, killButton;

    public CameraControl CameraTarget;

    public GameObject notificationPanel;
    public Text notificationText;

    void Update()
    {
        if (CameraTarget.currentCameraTarget != null) {
            AnimalManager target = CameraTarget.currentCameraTarget.GetComponent<AnimalManager>();
            detailPicture.sprite = CameraTarget.currentCameraTarget.GetComponent<SpriteRenderer>().sprite;
            detailName.text = target.data.name;
            detailAge.text = Mathf.Floor(target.data.age).ToString();
            detailBreed.text = target.data.type.ToString();
            if (target.data.gender == AnimalGender.Male) {
                detailSex.sprite = maleIcon;
            } else {
                detailSex.sprite = femaleIcon;
            }
            healthBar.value = target.data.health;
            foodBar.value = target.data.food;
            hygieneBar.value = target.data.hygiene;
            if (target.IsInShelter() == true) {
                healButton.gameObject.SetActive(true);
                feedButton.gameObject.SetActive(true);
                washButton.gameObject.SetActive(true);
                killButton.gameObject.SetActive(true);
                acceptButton.gameObject.SetActive(false);
            } else {
                healButton.gameObject.SetActive(false);
                feedButton.gameObject.SetActive(false);
                washButton.gameObject.SetActive(false);
                killButton.gameObject.SetActive(false);
                acceptButton.gameObject.SetActive(true);
            }
            //acceptButton.gameObject.SetActive(!target.IsInShelter());
            //killButton.gameObject.SetActive(target.IsInShelter());
            detailPanel.SetActive(true);
        } else {
            detailPanel.SetActive(false);
        }
    }

    public void feedTarget()
    {
        if (CameraTarget.currentCameraTarget != null) {
            AnimalManager target = CameraTarget.currentCameraTarget.GetComponent<AnimalManager>();
            target.Feed();
        }
    }

    public void washTarget()
    {
        if (CameraTarget.currentCameraTarget != null) {
            AnimalManager target = CameraTarget.currentCameraTarget.GetComponent<AnimalManager>();
            target.Wash();
        }
    }

    public void healTarget()
    {
        if (CameraTarget.currentCameraTarget != null) {
            AnimalManager target = CameraTarget.currentCameraTarget.GetComponent<AnimalManager>();
            target.Heal();
        }
    }

    public void killTarget()
    {
        if (CameraTarget.currentCameraTarget != null) {
            AnimalManager target = CameraTarget.currentCameraTarget.GetComponent<AnimalManager>();
            target.Kill();
            CameraTarget.currentCameraTarget = null;
        }
    }

    public void acceptTarget()
    {
        if (CameraTarget.currentCameraTarget != null) {
            AnimalManager target = CameraTarget.currentCameraTarget.GetComponent<AnimalManager>();
            if (target.AcceptInShelter())
            {
                acceptButton.gameObject.SetActive(false);
            }
            else
            {
                notificationPanel.SetActive(true);
                notificationText.text = "You do not have any box left for that animal!";
            }
        }
    }
}
