using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public GameObject cameraSphere;

    private void Update()
    {
        if (!GameManager.game.IsPaused())
        {
            // Keep the camera facing up and looking at the target
            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
            transform.LookAt(cameraSphere.transform.position);
        }
    }
}
