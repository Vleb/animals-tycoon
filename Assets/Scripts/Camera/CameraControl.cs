using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject defaultCameraTarget;

    public float cameraRotationSpeed = 5f;
    public float cameraMinAngle = 30f;
    public float cameraMaxAngle = 75f;
    public float cameraMinZoom = 10f;
    public float cameraMaxZoom = 200f;
    public float cameraMaxPosX = 100f;
    public float cameraMaxPosZ = 100f;
    public float cameraMaxClickDistance = 100f;

    [HideInInspector]
    public GameObject currentCameraTarget;

    private void Start()
    {
        // Set default camera target
        currentCameraTarget = null;
        transform.position = defaultCameraTarget.transform.position;

        // Set default rotation and zoom to be in the min max range
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, (cameraMinAngle + cameraMaxAngle) / 2f);
        float scale = (cameraMinZoom + cameraMaxZoom) / 2f;
        transform.localScale = new Vector3(scale, scale, scale);
    }

    private void Update()
    {
        if (!GameManager.game.IsPaused())
        {
            Vector2 mouseMovement = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

            // Set camera target
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit raycastHit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out raycastHit, cameraMaxClickDistance))
                {
                    if (raycastHit.transform != null && raycastHit.transform.gameObject.tag.StartsWith("Animal"))
                    {
                        currentCameraTarget = raycastHit.transform.gameObject;
                    }
                }
            }

            // Manage rotation
            if (Input.GetMouseButton(1))
            {
                float h = cameraRotationSpeed * mouseMovement.x;
                float v = cameraRotationSpeed * mouseMovement.y;
                if (transform.eulerAngles.z + v <= cameraMinAngle || transform.eulerAngles.z + v >= cameraMaxAngle)
                {
                    v = 0f;
                }
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + h, transform.eulerAngles.z + v);
            }

            // Manage zoom
            float scrollFactor = Input.GetAxis("Mouse ScrollWheel");
            if (scrollFactor != 0)
            {
                float scale = Mathf.Clamp(transform.localScale.x * (1f - scrollFactor), cameraMinZoom, cameraMaxZoom);
                transform.localScale = new Vector3(scale, scale, scale);
            }

            // Manage position
            if (currentCameraTarget != null)
            {
                if (Input.GetMouseButtonDown(2))
                {
                    currentCameraTarget = null;
                    transform.position = new Vector3(transform.position.x, defaultCameraTarget.transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = currentCameraTarget.transform.position;
                }
            }
            else
            {
                if (Input.GetMouseButton(2))
                {
                    Vector3 cameraMovement = Quaternion.Euler(0f, Camera.main.transform.eulerAngles.y, 0f) * new Vector3(-mouseMovement.x, 0.0f, -mouseMovement.y);
                    Vector3 cameraPosition = transform.position + cameraMovement * (transform.localScale.x / 200f);
                    transform.position = new Vector3(Mathf.Clamp(cameraPosition.x, -cameraMaxPosX, cameraMaxPosX), cameraPosition.y, Mathf.Clamp(cameraPosition.z, -cameraMaxPosZ, cameraMaxPosZ));
                }
            }
        }
    }
}
