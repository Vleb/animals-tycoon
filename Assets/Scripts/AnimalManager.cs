using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum AnimalGender
{
    Male,
    Female
}

public abstract class AnimalName
{
    public static string[] maleNameList = new string[] {
        "Max", "Charlie", "Cooper", "Buddy", "Milo", "Bear", "Rocky", "Duke", "Tucker", "Jack",
        "Oliver", "Teddy", "Leo", "Bentley", "Zeus", "Jax", "Toby", "Winston", "Ollie", "Louie",
        "Finn", "Murphy", "Moose", "Loki", "Gus", "Hank", "Koda", "Blue", "Apollo", "Dexter",
        "Beau", "Bruno", "Henry", "Thor", "Buster", "Ace", "Jake", "Maverick", "Diesel", "Bandit",
        "Archie", "Gunner", "Harley", "Bailey", "Lucky", "Jackson", "Benji", "Oscar", "Bo", "Scout",
        "Jasper", "Otis", "Simba", "Cash", "Baxter", "Kobe", "Shadow", "Tank", "Riley", "Rex",
        "Gizmo", "Sam", "Benny", "Brody", "Theo", "Sammy", "Rocco", "Cody", "Luke", "Boomer",
        "Ranger", "Roscoe", "Oakley", "George", "Copper", "King", "Ziggy", "Chance", "Marley", "Prince",
        "Oreo", "Odin", "Hunter", "Coco", "Zeke", "Chewy", "Bruce", "Tyson", "Frankie", "Chase",
        "Rudy", "Rusty", "Ruger", "Walter", "Romeo", "Joey", "Mac", "Remy", "Bubba", "Peanut"
    };

    public static string[] femaleNameList = new string[] {
        "Bella", "Luna", "Lucy", "Daisy", "Lola", "Sadie", "Molly", "Bailey", "Stella", "Maggie",
        "Chloe", "Penny", "Nala", "Zoey", "Lily", "Coco", "Sophie", "Rosie", "Ellie", "Ruby",
        "Piper", "Mia", "Roxy", "Gracie", "Millie", "Willow", "Lulu", "Pepper", "Ginger", "Harley",
        "Abby", "Winnie", "Nova", "Kona", "Riley", "Zoe", "Lilly", "Dixie", "Lady", "Izzy",
        "Hazel", "Layla", "Olive", "Charlie", "Sasha", "Maya", "Honey", "Athena", "Lexi", "Cali",
        "Annie", "Belle", "Princess", "Phoebe", "Emma", "Ella", "Cookie", "Marley", "Callie", "Scout",
        "Roxie", "Remi", "Minnie", "Maddie", "Dakota", "Leia", "Poppy", "Josie", "Harper", "Mila",
        "Angel", "Holly", "Ava", "Ivy", "Mocha", "Gigi", "Paisley", "Koda", "Cleo", "Penelope",
        "Bonnie", "Missy", "Frankie", "Sugar", "Aspen", "Xena", "Shelby", "Fiona", "Dolly", "Georgia",
        "Shadow", "Delilah", "Peanut", "Grace", "Rose", "Skye", "Pearl", "Jasmine", "Juno", "Trixie"
    };

    public static string Generate(AnimalGender gender)
    {
        return gender == AnimalGender.Male ? maleNameList[Random.Range(0, 100)] : femaleNameList[Random.Range(0, 100)];
    }
}

public enum AnimalType
{
    Cat,
    Dog,
    Duck,
    Rabbit,
    Mouse
}

[System.Serializable]
public class AnimalData
{
    public AnimalType type;
    public AnimalGender gender;
    public string name;
    public float age;
    public float food;
    public float hygiene;
    public float health;
    public float lifeExpectancy;
    public bool alive;
    public int colorVariant;
}

public class AnimalManager : MonoBehaviour
{
    public ParticleSystem dieEffect;
    public AudioSource dieSound;

    [HideInInspector]
    public UnityEvent dieEvent;

    [HideInInspector]
    public AnimalData data;

    [HideInInspector]
    public GameObject spawnOrigin;

    [HideInInspector]
    public GameObject roomAttached;

    private float spawnAge;

    IEnumerator RoutineSound()
    {
        yield return new WaitForSeconds(Random.Range(3f, 30f));
        if (data.alive)
        {
            GetComponent<AudioSource>().Play();
            StartCoroutine(RoutineSound());
        }
    }

    void Start()
    {
        dieEvent.AddListener(MakeDead);
        data = new AnimalData();

        // Set the random animal type
        List<AnimalType> possibleTypes = new List<AnimalType>();
        possibleTypes.Add(AnimalType.Cat);
        possibleTypes.Add(AnimalType.Dog);
        if (GameManager.game.data.duckUnlocked)
        {
            possibleTypes.Add(AnimalType.Duck);
        }
        if (GameManager.game.data.rabbitUnlocked)
        {
            possibleTypes.Add(AnimalType.Rabbit);
        }
        if (GameManager.game.data.mouseUnlocked)
        {
            possibleTypes.Add(AnimalType.Mouse);
        }
        data.type = possibleTypes[Random.Range(0, possibleTypes.Count)];

        // Set a gender and a fitting name
        data.gender = Random.value > .5f ? AnimalGender.Male : AnimalGender.Female;
        data.name = AnimalName.Generate(data.gender);

        // Set the age, the life expectancy, the food status and the health status of the animal when he appears
        switch (data.type)
        {
            case AnimalType.Cat:
                data.lifeExpectancy = 20f;
                break;
            case AnimalType.Dog:
                data.lifeExpectancy = 20f;
                break;
            case AnimalType.Duck:
                data.lifeExpectancy = 20f;
                break;
            case AnimalType.Rabbit:
                data.lifeExpectancy = 10f;
                break;
            case AnimalType.Mouse:
                data.lifeExpectancy = 3f;
                break;
        }
        data.lifeExpectancy *= Random.Range(.75f, 1.25f);
        data.age = Random.Range(.1f, data.lifeExpectancy * .9f);
        data.food = Random.Range(.1f, 1f);
        data.hygiene = Random.Range(.1f, 1f);
        data.health = Random.Range(.1f, 1f);
        data.alive = true;

        // Set random color to the animal
        Object[] spriteArray = Resources.LoadAll("Sprites/Animal/" + GetTypeAsString(), typeof(Sprite));
        if (spriteArray.Length > 0)
        {
            data.colorVariant = Random.Range(0, spriteArray.Length);
            GetComponent<SpriteRenderer>().sprite = Instantiate(spriteArray[data.colorVariant]) as Sprite;
        }

        // Set random sound to the animal
        if (data.type != AnimalType.Rabbit)
        {
            Object[] soundArray = Resources.LoadAll("Sounds/Animal/" + GetTypeAsString(), typeof(AudioClip));
            if (soundArray.Length > 0)
            {
                GetComponent<AudioSource>().clip = Instantiate(soundArray[Random.Range(0, soundArray.Length)]) as AudioClip;
                StartCoroutine(RoutineSound());
            }
        }

        spawnAge = Time.time;
    }

    void Update()
    {
        if (data.alive)
        {
            if (IsInShelter())
            {
                // Make the animal get older and grow if he isn't fully grown yet
                data.age += Time.deltaTime / 360f;
                float adultAge = 20f / data.lifeExpectancy;
                transform.localScale = Vector3.Lerp(new Vector3(.25f, .25f, .25f), new Vector3(1f, 1f, 1f), data.age * adultAge);

                // Manage food
                data.food = Mathf.Max(0f, data.food - Time.deltaTime / 60f);
                data.hygiene = Mathf.Max(0f, data.hygiene - Time.deltaTime / 60f);
                if (data.food == 0f || data.hygiene == 0f)
                {
                    data.health -= Time.deltaTime / 60f;
                }

                // Manage health
                if (data.health == 0f)
                {
                    data.lifeExpectancy -= Time.deltaTime / 360f;
                }

                // Manage death
                if (data.age > data.lifeExpectancy)
                {
                    dieEvent.Invoke();
                }
            }
            else if (Time.time - spawnAge > 60f)
            {
                MakeDespawn();
            }
        }
    }

    public string GetTypeAsString()
    {
        switch (data.type)
        {
            case AnimalType.Cat:
                return "Cat";
            case AnimalType.Dog:
                return "Dog";
            case AnimalType.Duck:
                return "Duck";
            case AnimalType.Rabbit:
                return "Rabbit";
            case AnimalType.Mouse:
                return "Mouse";
        }
        return null;
    }

    public static string GetTypeAsString(AnimalType type)
    {
        switch (type)
        {
            case AnimalType.Cat:
                return "Cat";
            case AnimalType.Dog:
                return "Dog";
            case AnimalType.Duck:
                return "Duck";
            case AnimalType.Rabbit:
                return "Rabbit";
            case AnimalType.Mouse:
                return "Mouse";
        }
        return null;
    }

    public string GetAgeAsString()
    {
        if (data.age < 1f)
        {
            int months = Mathf.FloorToInt(data.age * 100f / 12f);
            return months.ToString() + (months >= 2 ? " months" : " month") + " old";
        }
        else
        {
            int years = Mathf.FloorToInt(data.age);
            return years.ToString() + (years >= 2 ? " years" : " year") + " old";
        }
    }

    public bool Feed()
    {
        if (data.alive)
        {
            bool hasFood = false;
            switch (data.type)
            {
                case AnimalType.Cat:
                    hasFood = GameManager.game.data.catFood > 0;
                    if (hasFood)
                    {
                        GameManager.game.data.catFood--;
                    }
                    break;
                case AnimalType.Dog:
                    hasFood = GameManager.game.data.dogFood > 0;
                    if (hasFood)
                    {
                        GameManager.game.data.dogFood--;
                    }
                    break;
                case AnimalType.Duck:
                    hasFood = GameManager.game.data.duckFood > 0;
                    if (hasFood)
                    {
                        GameManager.game.data.duckFood--;
                    }
                    break;
                case AnimalType.Rabbit:
                    hasFood = GameManager.game.data.rabbitFood > 0;
                    if (hasFood)
                    {
                        GameManager.game.data.rabbitFood--;
                    }
                    break;
                case AnimalType.Mouse:
                    hasFood = GameManager.game.data.mouseFood > 0;
                    if (hasFood)
                    {
                        GameManager.game.data.mouseFood--;
                    }
                    break;
            }
            if (hasFood)
            {
                data.food = Mathf.Min(1f, data.food + .5f);
                return true;
            }
        }
        return false;
    }

    public bool Wash()
    {
        if (data.alive)
        {
            data.hygiene = 1f;
            return true;
        }
        return false;
    }

    public bool Heal()
    {
        if (data.alive && GameManager.game.data.medicine > 0)
        {
            data.health = 1f;
            GameManager.game.data.medicine--;
            return true;
        }
        return false;
    }

    public bool Kill()
    {
        if (data.alive)
        {
            dieEvent.Invoke();
            GameManager.game.data.prestige -= data.health * 2f;
            return true;
        }
        return false;
    }

    public bool IsInShelter()
    {
        return roomAttached != null;
    }

    private void MakeDisappear()
    {
        data.alive = false;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<SphereCollider>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        if (IsInShelter())
        {
            GameManager.game.data.animalList.Remove(data);
        }
        roomAttached = null;
    }

    private void MakeDespawn()
    {
        if (data.alive)
        {
            MakeDisappear();
            GameManager.game.data.prestige -= data.health / 2f;
        }
    }

    private void MakeDead()
    {
        if (data.alive)
        {
            data.alive = false;
            MakeDisappear();
            dieEffect.Play();
            dieSound.Play();
        }
    }

    public bool MakeAdopted()
    {
        if (data.alive)
        {
            MakeDisappear();
            GameManager.game.data.money += 100;
            GameManager.game.data.prestige += (data.health) / 3f;
            return true;
        }
        return false;
    }

    public bool AcceptInShelter()
    {
        if (data.alive && !IsInShelter())
        {
            GameObject[] rooms = GameObject.FindGameObjectsWithTag("Room" + GetTypeAsString());
            foreach (GameObject room in rooms)
            {
                if (room.activeSelf)
                {
                    GameObject[] animals = GameObject.FindGameObjectsWithTag("Animal");
                    bool isRoomFree = true;
                    foreach(GameObject animal in animals)
                    {
                        if (animal.GetComponent<AnimalManager>().roomAttached == room)
                        {
                            isRoomFree = false;
                            break;
                        }
                    }
                    if (isRoomFree)
                    {
                        roomAttached = room;
                        transform.localPosition = new Vector3(room.transform.position.x, room.transform.position.y + 3f, room.transform.position.z);
                        GameManager.game.data.animalList.Add(data);
                        GameManager.game.data.prestige += (1f - data.health) / 3f;
                        if (spawnOrigin != null)
                        {
                            spawnOrigin.SetActive(true);
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
