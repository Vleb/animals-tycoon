using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class GameData
{
    public float money = 1000f;
    public float prestige = 3f;

    public bool duckUnlocked = false;
    public bool rabbitUnlocked = false;
    public bool mouseUnlocked = false;

    public int catRoom = 2;
    public int dogRoom = 2;
    public int duckRoom = 0;
    public int rabbitRoom = 0;
    public int mouseRoom = 0;

    public List<AnimalData> animalList;

    public int catFood = 10;
    public int dogFood = 10;
    public int duckFood = 0;
    public int rabbitFood = 0;
    public int mouseFood = 0;

    public int medicine = 0;

    public float campainAbandonmentTimeLeft = 0f;
    public CampainSize campainAbandonmentSize = CampainSize.None;
    public float campainAdoptionTimeLeft = 0f;
    public CampainSize campainAdoptionSize = CampainSize.None;
    public float campainMistreatmentTimeLeft = 0f;
    public CampainSize campainMistreatmentSize = CampainSize.None;
    public float campainDonationTimeLeft = 0f;
    public CampainSize campainDonationSize = CampainSize.None;

    public GameData()
    {
        animalList = new List<AnimalData>();
    }

    public int CountAnimals()
    {
        return animalList.Count;
    }

    public int CountAnimals(AnimalType type)
    {
        int animalCount = 0;
        foreach (AnimalData animalData in animalList)
        {
            if (animalData.type == type)
            {
                animalCount++;
            }
        }
        return animalCount;
    }

    public int CountFood()
    {
        return catFood + dogFood + duckFood + rabbitFood + mouseFood;
    }

    public int CountRooms()
    {
        return catRoom + dogRoom + duckRoom + rabbitRoom + mouseRoom;
    }
}

public class GameManager : MonoBehaviour
{
    public static GameManager game;

    public GameObject animalPrefab;

    public GameObject adoptionPanel;
    public GameObject notificationPanel;
    public Text notificationText;
    public GameObject gameOverPanel;

    public AudioSource dingSound;
    public AudioSource moneySound;

    [HideInInspector]
    public UnityEvent eventSpawnAnimal;
    [HideInInspector]
    public UnityEvent eventSpawnAdoption;
    [HideInInspector]
    public UnityEvent eventSpawnDonation;

    [HideInInspector]
    public GameData data;

    private string saveName = "gamesave.save";
    private GameObject[] animalSpawners;
    private GameObject[] catRooms;
    private GameObject[] dogRooms;
    private GameObject[] duckRooms;
    private GameObject[] rabbitRooms;
    private GameObject[] mouseRooms;

    void Start()
    {
        game = this;

        eventSpawnAnimal.AddListener(SpawnAnimal);
        eventSpawnAdoption.AddListener(SpawnAdoption);
        eventSpawnDonation.AddListener(SpawnDonation);

        StartCoroutine(RoutineSpawnAnimal(20f, 40f));
        StartCoroutine(RoutineSpawnAdoption(40f, 60f));
        StartCoroutine(RoutineSpawnDonation(60f, 120f));

        if (File.Exists(Application.persistentDataPath + '/' + saveName))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Open(Application.persistentDataPath + '/' + saveName, FileMode.Open);
            data = (GameData)formatter.Deserialize(saveFile);
            saveFile.Close();
        }
        else
        {
            data = new GameData();
        }

        animalSpawners = GameObject.FindGameObjectsWithTag("Spawner");
        catRooms = GameObject.FindGameObjectsWithTag("RoomCat");
        dogRooms = GameObject.FindGameObjectsWithTag("RoomDog");
        duckRooms = GameObject.FindGameObjectsWithTag("RoomDuck");
        rabbitRooms = GameObject.FindGameObjectsWithTag("RoomRabbit");
        mouseRooms = GameObject.FindGameObjectsWithTag("RoomMouse");

        SpawnAnimal();
    }

    void Update()
    {
        // Rooms management
        for (int i = 0; i < catRooms.Length; i++)
        {
            catRooms[i].SetActive(i < data.catRoom);
        }
        for (int i = 0; i < dogRooms.Length; i++)
        {
            dogRooms[i].SetActive(i < data.dogRoom);
        }
        for (int i = 0; i < duckRooms.Length; i++)
        {
            duckRooms[i].SetActive(i < data.duckRoom);
        }
        for (int i = 0; i < rabbitRooms.Length; i++)
        {
            rabbitRooms[i].SetActive(i < data.rabbitRoom);
        }
        for (int i = 0; i < mouseRooms.Length; i++)
        {
            mouseRooms[i].SetActive(i < data.mouseRoom);
        }

        // Campains management
        if (data.campainAbandonmentTimeLeft > 0f)
        {
            data.campainAbandonmentTimeLeft -= Time.deltaTime;
        } 
        else
        {
            data.campainAbandonmentTimeLeft = 0f;
            data.campainAbandonmentSize = CampainSize.None;
        }
        if (data.campainAdoptionTimeLeft > 0f)
        {
            data.campainAdoptionTimeLeft -= Time.deltaTime;
        }
        else
        {
            data.campainAdoptionTimeLeft = 0f;
            data.campainAdoptionSize = CampainSize.None;
        }
        if (data.campainMistreatmentTimeLeft > 0f)
        {
            data.campainMistreatmentTimeLeft -= Time.deltaTime;
        }
        else
        {
            data.campainMistreatmentTimeLeft = 0f;
            data.campainMistreatmentSize = CampainSize.None;
        }
        if (data.campainDonationTimeLeft > 0f)
        {
            data.campainDonationTimeLeft -= Time.deltaTime;
        }
        else
        {
            data.campainDonationTimeLeft = 0f;
            data.campainDonationSize = CampainSize.None;
        }

        // Prestige management
        if (data.money < 0f)
        {
            data.prestige -= Time.deltaTime * Mathf.Abs(data.money) / 1000f;
        }
        else
        {
            data.money += Time.deltaTime * data.prestige;
        }
        if (data.campainMistreatmentSize > 0f)
        {
            data.prestige += Time.deltaTime * (float)data.campainMistreatmentSize / 60f;
        }
        data.prestige = Mathf.Clamp(data.prestige, 0f, 5f);
        if (data.prestige == 0f)
        {
            gameOverPanel.SetActive(true);
        }

        // Time management
        bool panelOnScreen = false;
        foreach (GameObject panel in GameObject.FindGameObjectsWithTag("UIPanel"))
        {
            if (panel.activeSelf)
            {
                panelOnScreen = true;
                Pause();
                break;
            }
        }
        if (!panelOnScreen)
        {
            Play();
        }
    }

    public void Save()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Create(Application.persistentDataPath + '/' + saveName);
        formatter.Serialize(saveFile, data);
        saveFile.Close();
    }

    // Spawn events
    IEnumerator RoutineSpawnAnimal(float minInterval, float maxInterval)
    {
        float r = Random.Range(minInterval, maxInterval);
        for (int i = 0; i < r * (1 + (int)data.campainAbandonmentSize); i++)
        {
            yield return new WaitForSeconds(1f);
        }
        eventSpawnAnimal.Invoke();
        StartCoroutine(RoutineSpawnAnimal(minInterval, maxInterval));
    }

    private void SpawnAnimal()
    {
        foreach (GameObject spawner in animalSpawners)
        {
            if (spawner.activeSelf)
            {
                dingSound.Play();
                spawner.SetActive(false);
                GameObject animal = Instantiate(animalPrefab, spawner.transform.position, Quaternion.identity);
                animal.GetComponent<AnimalManager>().spawnOrigin = spawner;
                break;
            }
        }
    }

    // Adoption events
    IEnumerator RoutineSpawnAdoption(float minInterval, float maxInterval)
    {
        float r = Random.Range(minInterval, maxInterval);
        for (int i = 0; i < r / (1 + (int)data.campainAdoptionSize); i++)
        {
            yield return new WaitForSeconds(1f);
        }
        eventSpawnAdoption.Invoke();
        StartCoroutine(RoutineSpawnAdoption(minInterval, maxInterval));
    }

    private void SpawnAdoption()
    {
        dingSound.Play();
        List<AnimalType> possibleTypes = new List<AnimalType>();
        possibleTypes.Add(AnimalType.Cat);
        possibleTypes.Add(AnimalType.Dog);
        if (data.duckUnlocked)
        {
            possibleTypes.Add(AnimalType.Duck);
        }
        if (data.rabbitUnlocked)
        {
            possibleTypes.Add(AnimalType.Rabbit);
        }
        if (data.mouseUnlocked)
        {
            possibleTypes.Add(AnimalType.Mouse);
        }
        adoptionPanel.GetComponent<AdoptionPanel>().UpdatePanel(possibleTypes[Random.Range(0, possibleTypes.Count)]);
        adoptionPanel.SetActive(true);
    }

    // Donation events
    IEnumerator RoutineSpawnDonation(float minInterval, float maxInterval)
    {
        float r = Random.Range(minInterval, maxInterval);
        for (int i = 0; i < r / (1 + (int)data.campainDonationSize); i++)
        {
            yield return new WaitForSeconds(1f);
        }
        eventSpawnDonation.Invoke();
        StartCoroutine(RoutineSpawnDonation(minInterval, maxInterval));
    }

    private void SpawnDonation()
    {
        moneySound.Play();
        int amount = Mathf.FloorToInt(Mathf.Pow(Random.Range(10f, 100f), 2f)) / 10;
        data.money += amount;
        notificationText.text = "You received a donation for an amount of " + amount + "�!";
        notificationPanel.SetActive(true);
    }

    // Manage game pause
    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void Play()
    {
        Time.timeScale = 1f;
    }

    public bool IsPaused()
    {
        return Time.timeScale < .5f;
    }
}