using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimalPanel : MonoBehaviour
{
    public GameObject animalUIPrefab;

    public GameObject tycoonCamera;
    public GameObject viewContent;
    public AudioSource click;

    public void UpdatePanel()
    {
        // Remove previous prefabs
        foreach (Transform child in viewContent.transform)
        {
            Destroy(child.gameObject);
        }

        // Update animals data
        float posY = 70f;
        float posYDelta = animalUIPrefab.GetComponent<RectTransform>().sizeDelta.y;
        GameObject[] animalList = GameObject.FindGameObjectsWithTag("Animal");
        for (int i = 0; i < GameManager.game.data.animalList.Count; i++)
        {
            // Instantiate prefabs
            GameObject animalUI = Instantiate(animalUIPrefab, viewContent.transform);
            animalUI.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -posY);
            posY += posYDelta;

            // Find reference to animal game object
            AnimalData animalData = GameManager.game.data.animalList[i];
            GameObject animalObject = null;
            foreach (GameObject animal in animalList)
            {
                if (animal.GetComponent<AnimalManager>().data == animalData)
                {
                    animalObject = animal;
                    break;
                }
            }

            // Find reference to all animal ui elements
            Image animalUIImage = animalUI.transform.Find("Image").gameObject.GetComponent<Image>();
            Text animalUISpecie = animalUI.transform.Find("Specie").gameObject.GetComponent<Text>();
            Text animalUIName = animalUI.transform.Find("Name").gameObject.GetComponent<Text>();
            Text animalUIAge = animalUI.transform.Find("Age").gameObject.GetComponent<Text>();
            Text animalUIStatus = animalUI.transform.Find("Status").gameObject.GetComponent<Text>();
            Button animalUIViewButton = animalUI.GetComponentInChildren<Button>();

            // Update ui elements
            if (animalObject != null)
            {
                animalUIImage.sprite = animalObject.GetComponent<SpriteRenderer>().sprite;
                animalUISpecie.text = animalObject.GetComponent<AnimalManager>().GetTypeAsString();
                animalUIName.text = animalData.name;
                animalUIAge.text = animalObject.GetComponent<AnimalManager>().GetAgeAsString();
                if (animalData.food < .5f)
                {
                    animalUIStatus.text = animalData.name + " is hungry.";
                }
                else if (animalData.health < .5f)
                {
                    animalUIStatus.text = animalData.name + " is feeling sick.";
                }
                else
                {
                    animalUIStatus.text = animalData.name + " is feeling good.";
                }
                animalUIViewButton.onClick.AddListener(delegate {
                    click.Play();
                    tycoonCamera.GetComponent<CameraControl>().currentCameraTarget = animalObject;
                    gameObject.SetActive(false);
                });
            }
        }
    }
}
