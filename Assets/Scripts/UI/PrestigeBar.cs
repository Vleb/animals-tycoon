using UnityEngine;
using UnityEngine.UI;

public class PrestigeBar : MonoBehaviour
{
    public Image[] starList;

    void Update()
    {
        int prestigeIdx = Mathf.FloorToInt(GameManager.game.data.prestige + .5f);
        for (int i = 0; i < starList.Length; i++)
        {
            starList[i].color = i < prestigeIdx ? new Color(200f, 200f, 0f) : new Color(0f, 0f, 0f);
        }
    }
}
