using UnityEngine;
using UnityEngine.UI;

public class DataBar : MonoBehaviour
{
    public Text animalText, foodText, medicineText, moneyText;

    void Update()
    {
        animalText.text = GameManager.game.data.CountAnimals().ToString() + '/' + GameManager.game.data.CountRooms();
        foodText.text = GameManager.game.data.CountFood().ToString();
        medicineText.text = GameManager.game.data.medicine.ToString();
        moneyText.text = Mathf.FloorToInt(GameManager.game.data.money).ToString() + '�';
    }
}
