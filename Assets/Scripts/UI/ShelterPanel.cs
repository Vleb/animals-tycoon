using UnityEngine;
using UnityEngine.UI;

public class ShelterPanel : MonoBehaviour
{
    public Text unlockedRabbit, unlockedMouse;
    public Text usedRoomCat, usedRoomDog, usedRoomRabbit, usedRoomMouse;
    public Text availableRoomCat, availableRoomDog, availableRoomRabbit, availableRoomMouse;

    void Update()
    {
        //unlockedDuck.text = GameManager.game.data.duckUnlocked ? "Yes" : "No";
        unlockedRabbit.text = GameManager.game.data.rabbitUnlocked ? "Yes" : "No";
        unlockedMouse.text = GameManager.game.data.mouseUnlocked ? "Yes" : "No";

        int catCount = 0;
        int dogCount = 0;
        //int duckCount = 0;
        int rabbitCount = 0;
        int mouseCount = 0;
        foreach (AnimalData animalData in GameManager.game.data.animalList)
        {
            switch (animalData.type)
            {
                case AnimalType.Cat:
                    catCount++;
                    break;
                case AnimalType.Dog:
                    dogCount++;
                    break;
                //case AnimalType.Duck:
                //    duckCount++;
                //    break;
                case AnimalType.Rabbit:
                    rabbitCount++;
                    break;
                case AnimalType.Mouse:
                    mouseCount++;
                    break;
            }
        }
        usedRoomCat.text = catCount.ToString();
        usedRoomDog.text = dogCount.ToString();
        //usedRoomDuck.text = duckCount.ToString();
        usedRoomMouse.text = mouseCount.ToString();
        usedRoomRabbit.text = rabbitCount.ToString();

        availableRoomCat.text = GameManager.game.data.catRoom.ToString();
        availableRoomDog.text = GameManager.game.data.dogRoom.ToString();
        //availableRoomDuck.text = GameManager.game.data.duckRoom.ToString();
        availableRoomRabbit.text = GameManager.game.data.rabbitRoom.ToString();
        availableRoomMouse.text = GameManager.game.data.mouseRoom.ToString();
    }
}
