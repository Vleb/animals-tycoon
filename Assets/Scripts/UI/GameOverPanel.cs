using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    void Start()
    {
        transform.Find("ButtonClose").gameObject.GetComponent<Button>().onClick.AddListener(delegate
        {
            GameManager.game.Play();
            SceneManager.LoadScene("MainMenu");
        });
    }
}
