using UnityEngine;
using UnityEngine.UI;

public class AdoptionPanel : MonoBehaviour
{
    public GameObject adoptionUIPrefab;

    public GameObject tycoonCamera;
    public Text infoText;
    public GameObject viewContent;
    public Button declineButton;
    public AudioSource click;

    public void UpdatePanel(AnimalType animalType)
    {
        // Update info text
        infoText.text = "A customer has arrived and wants to adopt a " + AnimalManager.GetTypeAsString(animalType) + "!\n" +
            "A successful adoption will give you money and reputation, but declining a customer will make you lose reputation.";

        // Remove previous prefabs
        foreach (Transform child in viewContent.transform)
        {
            Destroy(child.gameObject);
        }

        // Update animals data
        float posY = 70f;
        float posYDelta = adoptionUIPrefab.GetComponent<RectTransform>().sizeDelta.y;
        GameObject[] animals = GameObject.FindGameObjectsWithTag("Animal");
        foreach (GameObject animal in animals)
        {
            AnimalManager animalManager = animal.GetComponent<AnimalManager>();
            if (animalManager.data.type == animalType && animalManager.IsInShelter())
            {
                // Instantiate prefabs
                GameObject animalUI = Instantiate(adoptionUIPrefab, viewContent.transform);
                animalUI.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -posY);
                posY += posYDelta;

                // Find reference to all animal ui elements
                Image animalUIImage = animalUI.transform.Find("Image").gameObject.GetComponent<Image>();
                Text animalUIName = animalUI.transform.Find("Name").gameObject.GetComponent<Text>();
                Text animalUIAge = animalUI.transform.Find("Age").gameObject.GetComponent<Text>();
                Button animalUIAdoptButton = animalUI.GetComponentInChildren<Button>();

                // Update ui elements
                animalUIImage.sprite = animal.GetComponent<SpriteRenderer>().sprite;
                animalUIName.text = animalManager.data.name;
                animalUIAge.text = animalManager.GetAgeAsString();
                animalUIAdoptButton.onClick.AddListener(delegate {
                    click.Play();
                    tycoonCamera.GetComponent<CameraControl>().currentCameraTarget = null;
                    animalManager.MakeAdopted();
                    gameObject.SetActive(false);
                });
            }
        }
    }

    public void DeclineAdoption()
    {
        GameManager.game.data.prestige -= .5f;
        gameObject.SetActive(false);
    }
}
