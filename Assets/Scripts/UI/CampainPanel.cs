using UnityEngine;
using UnityEngine.UI;

public enum CampainSize
{
    None,
    Small,
    Medium,
    Big,
    Gigantic
}

public class CampainPanel : MonoBehaviour
{
    public Text campainAbandonmentStatus, campainAbandonmentTimeLeft, campainAbandonmentSize;
    public Button campainAbandonmentButton;
    public Text campainAbandonmentButtonText;

    public Text campainAdoptionStatus, campainAdoptionTimeLeft, campainAdoptionSize;
    public Button campainAdoptionButton;
    public Text campainAdoptionButtonText;

    public Text campainMistreatmentStatus, campainMistreatmentTimeLeft, campainMistreatmentSize;
    public Button campainMistreatmentButton;
    public Text campainMistreatmentButtonText;

    public Text campainDonationStatus, campainDonationTimeLeft, campainDonationSize;
    public Button campainDonationButton;
    public Text campainDonationButtonText;

    void Start()
    {
        campainAbandonmentButton.onClick.AddListener(CampainAbandonmentClicked);
        campainAdoptionButton.onClick.AddListener(CampainAdoptionClicked);
        campainMistreatmentButton.onClick.AddListener(CampainMistreatmentClicked);
        campainDonationButton.onClick.AddListener(CampainDonationClicked);
    }

    void Update()
    {
        // Campain abandonment
        if (GameManager.game.data.campainAbandonmentTimeLeft > 0f)
        {
            campainAbandonmentStatus.text = "Running";
            campainAbandonmentTimeLeft.text = Mathf.FloorToInt(GameManager.game.data.campainAbandonmentTimeLeft).ToString() + " seconds";
            campainAbandonmentSize.text = CampainSizeAsString(GameManager.game.data.campainAbandonmentSize);
            if (GameManager.game.data.campainAbandonmentSize == CampainSize.Gigantic)
            {
                campainAbandonmentButton.gameObject.SetActive(false);
            }
            else
            {
                campainAbandonmentButton.gameObject.SetActive(true);
                campainAbandonmentButtonText.text = "Upgrade campain";
            }
        }
        else
        {
            campainAbandonmentStatus.text = "Not Started";
            campainAbandonmentTimeLeft.text = "N/A";
            campainAbandonmentSize.text = "N/A";
            campainAbandonmentButton.gameObject.SetActive(true);
            campainAbandonmentButtonText.text = "Start campain";
        }

        // Campain adoption
        if (GameManager.game.data.campainAdoptionTimeLeft > 0f)
        {
            campainAdoptionStatus.text = "Running";
            campainAdoptionTimeLeft.text = Mathf.FloorToInt(GameManager.game.data.campainAdoptionTimeLeft).ToString() + " seconds";
            campainAdoptionSize.text = CampainSizeAsString(GameManager.game.data.campainAdoptionSize);
            if (GameManager.game.data.campainAdoptionSize == CampainSize.Gigantic)
            {
                campainAdoptionButton.gameObject.SetActive(false);
            }
            else
            {
                campainAdoptionButton.gameObject.SetActive(true);
                campainAdoptionButtonText.text = "Upgrade campain";
            }
        }
        else
        {
            campainAdoptionStatus.text = "Not Started";
            campainAdoptionTimeLeft.text = "N/A";
            campainAdoptionSize.text = "N/A";
            campainAdoptionButton.gameObject.SetActive(true);
            campainAdoptionButtonText.text = "Start campain";
        }

        // Campain mistreatment
        if (GameManager.game.data.campainMistreatmentTimeLeft > 0f)
        {
            campainMistreatmentStatus.text = "Running";
            campainMistreatmentTimeLeft.text = Mathf.FloorToInt(GameManager.game.data.campainMistreatmentTimeLeft).ToString() + " seconds";
            campainMistreatmentSize.text = CampainSizeAsString(GameManager.game.data.campainMistreatmentSize);
            if (GameManager.game.data.campainMistreatmentSize == CampainSize.Gigantic)
            {
                campainMistreatmentButton.gameObject.SetActive(false);
            }
            else
            {
                campainMistreatmentButton.gameObject.SetActive(true);
                campainMistreatmentButtonText.text = "Upgrade campain";
            }
        }
        else
        {
            campainMistreatmentStatus.text = "Not Started";
            campainMistreatmentTimeLeft.text = "N/A";
            campainMistreatmentSize.text = "N/A";
            campainMistreatmentButton.gameObject.SetActive(true);
            campainMistreatmentButtonText.text = "Start campain";
        }

        // Campain donation
        if (GameManager.game.data.campainDonationTimeLeft > 0f)
        {
            campainDonationStatus.text = "Running";
            campainDonationTimeLeft.text = Mathf.FloorToInt(GameManager.game.data.campainDonationTimeLeft).ToString() + " seconds";
            campainDonationSize.text = CampainSizeAsString(GameManager.game.data.campainDonationSize);
            if (GameManager.game.data.campainDonationSize == CampainSize.Gigantic)
            {
                campainDonationButton.gameObject.SetActive(false);
            }
            else
            {
                campainDonationButton.gameObject.SetActive(true);
                campainDonationButtonText.text = "Upgrade campain";
            }
        }
        else
        {
            campainDonationStatus.text = "Not Started";
            campainDonationTimeLeft.text = "N/A";
            campainDonationSize.text = "N/A";
            campainDonationButton.gameObject.SetActive(true);
            campainDonationButtonText.text = "Start campain";
        }
    }

    private void CampainAbandonmentClicked()
    {
        GameManager.game.data.money -= 300f;
        GameManager.game.data.campainAbandonmentTimeLeft += 60f;
        GameManager.game.data.campainAbandonmentSize++;
    }

    private void CampainAdoptionClicked()
    {
        GameManager.game.data.money -= 300f;
        GameManager.game.data.campainAdoptionTimeLeft += 60f;
        GameManager.game.data.campainAdoptionSize++;
    }

    private void CampainMistreatmentClicked()
    {
        GameManager.game.data.money -= 300f;
        GameManager.game.data.campainMistreatmentTimeLeft += 60f;
        GameManager.game.data.campainMistreatmentSize++;
    }

    private void CampainDonationClicked()
    {
        GameManager.game.data.money -= 300f;
        GameManager.game.data.campainDonationTimeLeft += 60f;
        GameManager.game.data.campainDonationSize++;
    }

    public static string CampainSizeAsString(CampainSize size)
    {
        switch (size)
        {
            case CampainSize.None:
                return "None";
            case CampainSize.Small:
                return "Small";
            case CampainSize.Medium:
                return "Medium";
            case CampainSize.Big:
                return "Big";
            case CampainSize.Gigantic:
                return "Gigantic";
        }
        return null;
    }
}
